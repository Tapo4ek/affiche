#### Описание задачи можно скачать [тут](https://bitbucket.org/Tapo4ek/affiche/src/90a3d4413bad690ef580f7dab3ec5b9c5aeb797b/%D0%A2%D0%B5%D1%81%D1%82%D0%BE%D0%B2%D0%BE%D0%B5%20%D0%B7%D0%B0%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5.pdf?at=master)

### Запуск проекта:

1. Настроить базу данных и внести нужные настройки в `settings.py`
```
CREATE EXTENSION postgis;
```

2. Запуск проекта.
```
psql -d affiche -f spatial_ref_sys.sql
pip install -r requirements.txt
python manage.py syncdb
```