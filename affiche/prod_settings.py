# -*- coding: utf-8 -*-


import os


__author__ = 'Tapo4ek <infdods@yandex.ru>'


DEBUG = False
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
STATIC_ROOT = os.path.join(BASE_DIR, "static")