Ext.define('Affiche.controller.Main', {
	extend: 'Ext.app.Controller',
	views: ['Viewport'],
	init: function() {
		var me = this;
        me.category = 'all';
		this.control({
            '#eventsGridpanel': {
				afterrender: function(grid) {
                    me.page = 1;
                    me.getEvents();

				},
                select: function(grid, record, index, eOpts ) {
                    me.getDescriptionData(record.data.id);
                }
			},
            '#backButton': {
                click: function(btn, e, eOpts ) {
                    me.page -= 1;
                    me.getEvents();
                }
            },
            '#nextButton': {
                click: function(btn, e, eOpts ) {
                    me.page += 1;
                    me.getEvents()
                }
            },
            '#categoryComboBox': {
                change: function( combo, newValue, oldValue, eOpts ) {
                    me.category = newValue;
                    me.getEvents();
                }
            }
		})
	},

    getDescriptionData: function(pk) {
        var rp = Ext.getCmp('renderPlace');
        rp.removeAll();

        Ext.Ajax.request({
            url: 'api/events/' + pk + '/',
            success: function(response, opts) {
                var obj = Ext.decode(response.responseText);
                var dp = Ext.create('Affiche.view.DescriptionPanel', {
                    fielfsetTitle: obj.title,
                    fieldsetDescription: obj.description,
                    margin: '-1 -1 -1 -1',
                    places: Ext.pluck(Ext.pluck(obj.instances, 'place'), 'name'),
                    cities: Ext.pluck(Ext.pluck(Ext.pluck(obj.instances, 'place'), 'city'), 'name')

                });
                Ext.getCmp('itemGrid').getStore().loadData(obj.instances);
                rp.add(dp);

            },
            failure: function(response, opts){
                console.error(response);
            }
        });
    },
    getEvents: function() {
        var me = this;
        var grid = Ext.getCmp('eventsGridpanel');
        var store = grid.getStore();
        Ext.Ajax.request({
            url: 'api/events/?per_page=' + store.pageSize + '&page=' + me.page + '&category=' + me.category,
            success: function(response, opts) {
                var obj = Ext.decode(response.responseText);
                grid.getStore().loadData(obj.data);
                Ext.getCmp('backButton').setDisabled(!obj.pagination.has_prev);
                Ext.getCmp('nextButton').setDisabled(!obj.pagination.has_next);
            },
            failure: function(response, opts){
                console.error(response);
            }
        });
    }
});
