Ext.define('Affiche.store.Categories', {
	extend: 'Ext.data.Store',
	model: 'Affiche.model.Categories',
	requires: [
        'Affiche.helper.DjangoProxy'
    ],
    autoLoad: true,
    autoSync: false,
    remoteSort: true,
	pageSize: 10,

	proxy: {
		type: 'django',
		url: '/api/categories/',
        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success'
        }
	}
});
