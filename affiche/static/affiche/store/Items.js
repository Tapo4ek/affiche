Ext.define('Affiche.store.Items', {
	extend: 'Ext.data.Store',
	model: 'Affiche.model.Items',
	requires: [
        'Affiche.helper.DjangoProxy'
    ],
    autoLoad: false,
    autoSync: false,
	proxy: {
		type: 'django',
		url: '/api/events/'
	}
});
