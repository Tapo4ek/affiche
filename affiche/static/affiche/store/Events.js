Ext.define('Affiche.store.Events', {
	extend: 'Ext.data.Store',
	model: 'Affiche.model.Events',
	requires: [
        'Affiche.helper.DjangoProxy'
    ],
    autoLoad: false,
    autoSync: false,
    remoteSort: true,
	pageSize: 10,

	proxy: {
		type: 'django',
		url: '/api/events/',
        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success'
        }
	}
});
