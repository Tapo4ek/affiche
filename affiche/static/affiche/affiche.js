Ext.application({
	name: 'Affiche',
	appFolder : '/static/affiche',
	controllers: ['Main'],
	stores: ['Events', 'Items', 'Categories'],
	requires: [
		'Affiche.helper.CrsfTokenHelper',
		'Affiche.helper.PhantomStoreInserter'
	],

	launch: function () {
        Ext.create('Affiche.view.Viewport').show();
	}
});
