Ext.define('Affiche.view.DescriptionPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.descriptionpanel',
    fielfsetTitle: 'Test',
    fieldsetDescription: 'sfglkdsfs',
    requires: [
        'Affiche.view.ItemGrid'
    ],
    initComponent: function() {
        var me = this;
        Ext.apply(this, {
            title: '',
            items: [
                {
                    xtype: 'fieldset',
                    margin: '5 5 5 5',
                    layout: 'fit',
                    title: me.fielfsetTitle,
                    defaultType: 'container',
                    items: [
                        {
                            html: me.fieldsetDescription,
                            margin: {
                                bottom: 5
                            }
                        }
                    ]

                },
                {
                    xtype: 'itemgrid',
                    id: 'itemGrid',
                    margin: '5 5 5 5',
                    cities: me.cities,
                    places: me.places
                }
            ]
        });

        this.callParent(arguments);
    }
});
