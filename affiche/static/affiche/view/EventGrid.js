Ext.define('Affiche.view.EventGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.eventgrid',
    flex: 1,
    autoscroll: true,
    multiSelect: false,
    initComponent: function() {
        var me = this;

        Ext.apply(this, {
            hideHeaders: true,
            flex: 1,
            title: 'Events',
            store: 'Events',
            tools: [
                {
                    fieldLabel: 'Category',
                    id: 'categoryComboBox',
                    xtype: 'combobox',
                    displayField: 'name',
                    valueField: 'id',
                    queryMode: 'local',
                    store: 'Categories',
                    margin: '0 5 0 5'
                }
            ],
            columns: [
                {
                    text: "Event",
                    flex: 1,
                    dataIndex: "title"
                }
            ],
            dockedItems: [
                {
                    xtype: 'panel',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'button',
                            text: '<< Back',
                            id: 'backButton',
                            disabled: true
                        },
                        {
                            xtype: 'button',
                            text: 'Next >>',
                            id: 'nextButton',
                            disabled: true
                        }
                    ]
                }
                //{
                //    xtype: 'pagingtoolbar',
                //    store: 'Events',
                //    dock: 'bottom',
                //    displayInfo: false,
                //    displayMsg: ''
                //}
            ]
        });

        this.callParent(arguments);
    }
});