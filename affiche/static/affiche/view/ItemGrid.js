Ext.define('Affiche.view.ItemGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.itemgrid',
    titleAlign: 'center',
    autoscroll: true,
    multiSelect: false,
    columns: [],
    initComponent: function() {
        var me = this;

        this.store = Ext.create('Ext.data.Store', {
            autoLoad: false,
            fields: me.fields,
            proxy: {
                params: {},
                type: 'ajax',
                url: 'feature/',
                timeout: 1000000,
                pageParam: false, //to remove param "page"
                startParam: false, //to remove param "start"
                limitParam: false, //to remove param "limit"
                noCache: false, //to remove param "_dc",
                actionMethods: {
                    read: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    successProperty: 'success'
                },
                writer: 'json'
            }
        });

        Ext.apply(this, {
            hideHeaders: false,
            header: false,
            flex: 1,
            store: 'Items',
            columns: [
                {
                    text: "Start date",
                    flex: 1,
                    dataIndex: "start"
                },
                {
                    text: "Place name",
                    flex: 1,
                    dataIndex: "place",
                    renderer: function(value, metaData, record, row, col, store, gridView) {
                        return me.places[row]
                    }
                },
                {
                    text: "City",
                    flex: 1,
                    dataIndex: "city",
                    renderer: function(value, metaData, record, row, col, store, gridView) {
                        return me.cities[row]
                    }
                }
            ]
        });
        this.callParent(arguments);
    }
});