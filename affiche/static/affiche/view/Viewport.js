Ext.define('Affiche.view.Viewport', {
	layout: 'fit',
	extend: 'Ext.container.Container',
	renderTo: Ext.getBody(),
	id: "eventContainer",
    requires: [
        'Affiche.view.DescriptionPanel',
        'Affiche.view.EventGrid'
    ],
	width: '100%',
	height: '100%',
	items: [
        {
            xtype: 'container',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'eventgrid',
                    id: 'eventsGridpanel'

                },
                {
                    xtype: 'panel',
                    layout: 'fit',
                    title: 'Description',
                    flex: 1,
                    id: 'renderPlace'
                }
            ]
        }
    ]
});
