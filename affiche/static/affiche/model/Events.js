Ext.define('Affiche.model.Events', {
	extend: 'Ext.data.Model',
	idProperty: 'id',
	fields: [
		{name: 'name', type: 'string'}
	]
});
