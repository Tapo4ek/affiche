Ext.define('Affiche.model.Items', {
	extend: 'Ext.data.Model',
	fields: [
		{name: 'start', type: 'string'},
		{name: 'place', type: 'string'},
		{name: 'city', type: 'string'}
	]
});
