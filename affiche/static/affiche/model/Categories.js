Ext.define('Affiche.model.Categories', {
	extend: 'Ext.data.Model',
	idProperty: 'id',
	fields: [
		{name: 'id', type: 'integer'},
		{name: 'name', type: 'string'}
	]
});
