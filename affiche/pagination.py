# -*- coding: utf-8 -*-


from rest_framework import pagination
from rest_framework.response import Response


__author__ = 'Tapo4ek <infdods@yandex.ru>'


class AffichePagination(pagination.PageNumberPagination):

    page_size_query_param = 'per_page'

    def get_paginated_response(self, data):
        return Response({
            'pagination': {
                "has_next": self.page.has_next(),
                "has_prev": self.page.has_previous()
            },
            'data': data
        })