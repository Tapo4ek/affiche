# -*- coding: utf-8 -*-


from django.contrib import admin
from event.models import Event, Instance, Category


__author__ = 'Tapo4ek <infdods@yandex.ru>'


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    pass


@admin.register(Instance)
class InstanceAdmin(admin.ModelAdmin):
    list_display = ('event', 'place', 'start', 'end')