# -*- coding: utf-8 -*-


from django.conf.urls import url
from event import views as eviews


__author__ = 'Tapo4ek <infdods@yandex.ru>'


urlpatterns = [
    url(r'^events/$', eviews.EventListView.as_view(), name='event-list'),
    url(r'^categories/$', eviews.CategoryListView.as_view(), name='category-list'),
    url(r'^events/geosearch/$', eviews.EventGeoListView.as_view(), name='event-geosearch'),
    url(r'^events/(?P<pk>\d+)/$', eviews.EventDetailView.as_view(), name='event-detail'),
]