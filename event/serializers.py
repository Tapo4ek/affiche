# -*- coding: utf-8 -*-


from rest_framework import serializers
from event import models as emodels
from location import models as lmodels
from location.serializers import PlaceSerializer


__author__ = 'Tapo4ek <infdods@yandex.ru>'


class EventListSerializer(serializers.ModelSerializer):

    class Meta:
        model = emodels.Event
        fields = (
            'id',
            'title',
            'description',
        )


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = emodels.Category
        fields = ('id', 'name')


class InstanceSerializer(serializers.ModelSerializer):

    place = PlaceSerializer(many=False)
    start = serializers.DateTimeField(format='%Y-%m-%d')

    class Meta:
        model = emodels.Instance
        fields = ('id', 'start', 'end', 'place')


class EventSerializer(serializers.ModelSerializer):

    instances = InstanceSerializer(many=True)

    def update(self, instance, validated_data):
        instance.title = validated_data['title']
        instance.description = validated_data['description']
        instance.save()
        return instance

    class Meta:
        model = emodels.Event
        fields = (
            'id',
            'title',
            'description',
            'instances'
        )
