# -*- coding: utf-8 -*-

import re
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.geos import fromstr
from rest_framework import generics
from rest_framework import mixins
from event.models import Event, Category, Instance
from location.models import Place
from event.serializers import EventListSerializer, EventSerializer, CategorySerializer


__author__ = 'Tapo4ek <infdods@yandex.ru>'


class CategoryListView(mixins.CreateModelMixin, generics.ListAPIView):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()


class EventListView(mixins.CreateModelMixin, generics.ListAPIView):
    serializer_class = EventListSerializer
    paginate_by = 10
    filter_fields = ('category', )

    def get_queryset(self):
        category = self.request.query_params.get('category', '')
        if category.isdigit():
            return Event.objects.filter(category_id=category)
        return Event.objects.all()


class EventGeoListView(generics.ListAPIView):
    serializer_class = EventListSerializer
    paginate_by = 10

    @staticmethod
    def check_value(val):
        """
        Проверяю что строка является либо целым либо float
        """
        return re.match('^(\-?\d+)(\.\d+)?$', val) is not None

    def get_queryset(self):
        lat = self.request.query_params.get('lat', '')
        lng = self.request.query_params.get('long', '')
        radius = self.request.query_params.get('radius', '')
        if EventGeoListView.check_value(lat) and EventGeoListView.check_value(lng):
            # point = GEOSGeometry('POINT({0} {1})'.format(lat, lng))
            point = fromstr('POINT({0} {1})'.format(lat, lng), srid=4326)

            if EventGeoListView.check_value(radius):
                # places = Place.objects.filter(point__distance_lte=(point, radius)).all()
                instances = Instance.objects.filter(place__point__distance_lte=(point, radius)).all()
                return Event.objects.filter(instances__in=instances)

        return Event.objects.all()

    def list(self, request, *args, **kwargs):
        return super(EventGeoListView, self).list(request, *args, **kwargs)


class EventDetailView(mixins.RetrieveModelMixin, mixins.UpdateModelMixin, generics.GenericAPIView):

    serializer_class = EventSerializer
    queryset = Event.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)
