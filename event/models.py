# -*- coding: utf-8 -*-


from django.db import models
from django.utils.translation import ugettext_lazy as _


__author__ = 'Tapo4ek <infdods@yandex.ru>'


class Category(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('Name'))

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')


class Event(models.Model):

    title = models.CharField(max_length=255, verbose_name=_('Title'))
    description = models.TextField(verbose_name=_('Description'))
    category = models.ForeignKey('event.Category', verbose_name=_('Category'), related_name='events')

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _('Event')
        verbose_name_plural = _('Events')


class Instance(models.Model):
    start = models.DateTimeField(blank=True)
    end = models.DateTimeField(blank=True)
    event = models.ForeignKey('event.Event', verbose_name=_('Event'), related_name='instances')
    place = models.ForeignKey('location.Place', verbose_name=_('Place'))

    class Meta:
        verbose_name = _('Instance')
        verbose_name_plural = _('Instances')
