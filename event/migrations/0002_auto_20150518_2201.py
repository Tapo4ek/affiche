# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models, migrations
from django.core.management import call_command


def load_default_data(apps, schema_editor):
    # We can't import the Person model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    # Person = apps.get_model("yourappname", "Person")
    call_command("loaddata", "event_default_data.json")


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0001_initial'),
        ('location', '0002_auto_20150518_2159'),
    ]

    operations = [
        migrations.RunPython(load_default_data),
    ]
