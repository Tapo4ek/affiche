# -*- coding: utf-8 -*-


from django.contrib import admin
from location.models import City, Place


__author__ = 'Tapo4ek <infdods@yandex.ru>'


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    pass


@admin.register(Place)
class PlaceAdmin(admin.ModelAdmin):
    pass
