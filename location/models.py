# -*- coding: utf-8 -*-


from django.contrib.gis.db import models
from django.utils.translation import ugettext_lazy as _


__author__ = 'Tapo4ek <infdods@yandex.ru>'


class City(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('Name'))

    class Meta:
        verbose_name = _('City')
        verbose_name_plural = _('Cities')

    def __unicode__(self):
        return self.name


class Place(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('Name'))
    point = models.PointField()
    city = models.ForeignKey('location.City', verbose_name=_('City'))
    objects = models.GeoManager()

    class Meta:
        verbose_name = _('Place')
        verbose_name_plural = _('Places')

    def __unicode__(self):
        return self.name