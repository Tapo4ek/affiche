# -*- coding: utf-8 -*-


from rest_framework import serializers
from location import models as lmodels


__author__ = 'Tapo4ek <infdods@yandex.ru>'


class CitySerializer(serializers.ModelSerializer):

    class Meta:
        model = lmodels.City
        fields = (
            'id',
            'name',
        )


class PlaceSerializer(serializers.ModelSerializer):

    city = CitySerializer(many=False)
    lat = serializers.SerializerMethodField()
    long = serializers.SerializerMethodField()

    def get_lat(self, obj):
        return obj.point.coords[0]

    def get_long(self, obj):
        return obj.point.coords[1]

    class Meta:
        model = lmodels.Place
        fields = (
            'id',
            'name',
            'lat',
            'long',
            'city'
        )